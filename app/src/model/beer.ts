import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, PrimaryColumn} from 'typeorm'

export interface Beer {
    id?: number;
    name: string;
    brewery: string;
    country: string;
    price: number;
    currency: string;
}

@Entity()
export class BeerItem extends BaseEntity implements Beer{
    @PrimaryColumn()
    id: number;

    @Column()
    name: string;

    @Column({nullable: true})
    brewery: string;

    @Column({nullable: true})
    country: string;

    @Column()
    price: number;

    @Column({nullable: true})
    currency: string;

    constructor(id: number, name: string, brewery:string, country:string, price:number, currency:string){
        super();
        this.id = id;
        this.name = name;
        this.brewery = brewery;
        this.country = country;
        this.price = price;
        this.currency = currency;
    }
}